<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Ibraheem Jarrar',
            'email' => 'ibraheem.jarrar@test.com',
            'password' => bcrypt('test123456'),
            'referral_code' => substr(md5(time()),0,12),
            'image' => 'images/admin.jpg',
            'role' => 'Admin'
        ]);

    }
}
