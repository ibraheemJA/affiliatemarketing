<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReferredUser extends Model
{
    use HasFactory;

    protected $table = 'referred_users';

    protected $fillable = ['referrer_user_id', 'referred_user_id'];

    public function referred_user()
    {
        return $this->belongsTo(User::class, 'referred_user_id');
    }
}
