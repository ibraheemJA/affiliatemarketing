<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'image',
        'referral_code',
        'referral_views',
        'role'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * A user has a referrer.
     *
     */
    public function referrer()
    {
        return $this->belongsTo(ReferredUser::class, 'referred_user_id', 'id');
    }

    /**
     * A user has many referrals.
     *
     */
    public function referrals()
    {
        return $this->hasMany(ReferredUser::class, 'referrer_user_id', 'id');
    }

    public function setImageAttribute($value)
    {
        if (request()->has('image')) {
            $destination_path = "images/profile_pictures";

            $imageName = time() . '.' . request()->image->extension();

            request()->image->move(base_path('public/' . $destination_path), $imageName);

            $this->attributes['image'] = $destination_path . '/' . $imageName;
        } else {
            $this->attributes['image'] = $value;
        }
    }
}
