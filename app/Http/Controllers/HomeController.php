<?php

namespace App\Http\Controllers;

use App\Models\ReferredUser;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function userPage()
    {
        $referred_users = ReferredUser::where('referrer_user_id', Auth::user()->id)->paginate(10);

        $chart_data = [];
        for ($i = 0; $i <= 14; $i++) {
            $date = Carbon::now()->subDays($i);
            $dayCount = ReferredUser::where('referrer_user_id', Auth::user()->id)
                ->whereBetween('created_at', [$date->startOfDay()->toDateTimeString(), $date->endOfDay()->toDateTimeString()])
                ->get();

            $chart_data['labels'][$i] = $date->toDateString();
            $chart_data['data'][$i] = $dayCount->count();
        }

        return view('userPage', compact('referred_users','chart_data'));
    }

    public function adminPage()
    {
        $users = User::select('id','email','name','created_at')->paginate(10);

        return view('adminPage',compact('users'));
    }
}
