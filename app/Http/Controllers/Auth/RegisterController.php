<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;
use Illuminate\Support\Facades\Validator;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'confirmed', Password::min(8)->letters()->numbers()],
            'ref' => ['required', 'exists:users,referral_code'],
            'image' => ['required', 'mimes:jpeg,png,jpg', 'max:5120']
        ], [
            'ref.exists' => 'Referral code doesn\'t exists',
            'ref.required' => 'You can\'t register without a referral link',
            'image.mimes' => 'Please insert image only',
            'image.max' => 'Image should be less than 5 MB'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'role' => 'User',
            'referral_code' => substr(md5(time()), 0, 12),
            'image' => $data['image'],
        ]);

        $referrer_user = User::select('id')->where('referral_code', $data['ref'])->first();

        // insert to referred_users table
        $user->referrer()->create([
            'referred_user_id' => $user->id,
            'referrer_user_id' => $referrer_user->id
        ]);

        return $user;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\View\View
     */
    public function showRegistrationForm(Request $request)
    {
        $ref_code = $request->ref ?? '';


        $referrer_user = User::where('referral_code', $ref_code)->first();
        if ($referrer_user) {
            $referrer_user->increment('referral_views');
        }
        return view('auth.register', compact('ref_code'));
    }

}
