@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        <div class="referral-code">
                            <h5>Copy this link to refer people :
                                <a href="">
                                    {{\URL::to('register') . '?ref=' . \Auth::user()->referral_code  }}
                                </a>
                            </h5>
                            <hr>
                            <div class="referred-users">
                                <h5>Here is list of users referred by you : </h5>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col">User</th>
                                        <th scope="col">Registration Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($referred_users as $referred_user)
                                        <tr>
                                            <th scope="row">{{$referred_user->referred_user->name}}</th>
                                            <td>{{$referred_user->created_at}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{$referred_users->links()}}
                            </div>
                            <hr>
                            <div class="referral-stats">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col">Referral Page Views</th>
                                        <th scope="col">Number of users registered through your link</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">{{\Auth::user()->referral_views}}</th>
                                        <td>{{\Auth::user()->referrals()->count()}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <hr>
                            <div class="referral-chart">
                                <div>
                                    <canvas id="myChart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        const data = {
            labels: @json($chart_data['labels']),
            datasets: [{
                label: 'Referral Chart',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: @json($chart_data['data']),
            }]
        };

        const config = {
            type: 'line',
            data: data,
            options: {}
        };


        var myChart = new Chart(
            document.getElementById('myChart'),
            config
        );
    </script>
@endsection

